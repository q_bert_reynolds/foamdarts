﻿Shader "InterTech/Bumped Anisotropic Specular" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        _HighlightColor ("Spec Color", Color) = (1,1,1,1)
        _MainTex ("Diffuse (RGB) Alpha (A)", 2D) = "white" {}
        _Smoothness ("Smoothness", Range(0,1)) = 0
        _AnisoTex ("Anisotropic Direction (RGB)", 2D) = "bump" {}
        _AnisoOffset ("Anisotropic Highlight Offset", Range(-1,1)) = -0.2
        _AnisoPower ("Anistrophic Strength", Float) = 128
        _Cutoff ("Alpha Cut-Off Threshold", Range(0,1)) = 0.5
    }

    SubShader{
        Tags { "RenderType" = "Opaque" }
    
        CGPROGRAM
        
            struct SurfaceOutputAniso {
                fixed3 Albedo;
                fixed3 Normal;
                fixed4 AnisoDir;
                fixed3 Specular;
                fixed Gloss;
                fixed Alpha;
                fixed3 Emission;
            };

            float _Smoothness, _AnisoOffset, _AnisoPower, _Cutoff;
            inline fixed4 LightingAniso (SurfaceOutputAniso s, fixed3 lightDir, fixed3 viewDir, fixed atten)
            {
                fixed3 h = normalize(lightDir + viewDir);
                float NdotL = saturate(dot(s.Normal, lightDir));
                fixed HdotA = dot(normalize(s.Normal + s.AnisoDir.rgb), h);
                float NdotH = saturate(dot(s.Normal, h));
                float aniso = max(0, sin(radians((HdotA + _AnisoOffset) * 180)));

                float3 spec = NdotH * _SpecColor;
                spec = saturate(pow(lerp(spec, aniso, s.AnisoDir.a), s.Gloss * _AnisoPower) * s.Specular);

                float3 reflectionDir = reflect(viewDir, s.Normal);
                float roughness = _Smoothness;
                float4 reflection = UNITY_SAMPLE_TEXCUBE_LOD(unity_SpecCube0, reflectionDir, roughness * UNITY_SPECCUBE_LOD_STEPS);
                float3 hdrReflection = DecodeHDR(reflection, unity_SpecCube0_HDR);
            
                fixed4 c;
                float3 light = _LightColor0.rgb * atten * 2 + hdrReflection;
                c.rgb = ((s.Albedo * light * NdotL) + (light * spec));
                c.a = 1;
                clip(s.Alpha - _Cutoff);
                return c;
            }

            #pragma surface surf Aniso
            #pragma target 3.0
            
            struct Input
            {
                float2 uv_MainTex;
                float2 uv_AnisoTex;
                float3 worldRefl;
                INTERNAL_DATA
            };
            
            sampler2D _MainTex, _AnisoTex;
            float4 _Color, _HighlightColor;

            void surf (Input IN, inout SurfaceOutputAniso o)
            {
                fixed4 albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;
                o.Albedo = albedo.rgb;
                o.Alpha = albedo.a;
                o.Specular = _HighlightColor * _Smoothness;
                o.Gloss = _Smoothness;
                o.AnisoDir = fixed4(tex2D(_AnisoTex, IN.uv_AnisoTex).rgb, 1);
            }
        ENDCG
    }
    FallBack "Transparent/Cutout/VertexLit"
}