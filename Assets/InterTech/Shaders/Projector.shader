﻿Shader "InterTech/Projector" {
     Properties{
         _Color("Tint Color", Color) = (1,1,1,1)
         _MainTex("Cookie", 2D) = "gray" {}
     }
     Subshader{
         Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
         Pass {
             ZWrite Off
             Offset -1, -1
             ColorMask RGB
             Blend SrcAlpha One // Additive blending
 
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
             #include "UnityCG.cginc"
 
             struct v2f {
                 float4 uv : TEXCOORD0;
                 float4 pos : SV_POSITION;
             };
 
             float4x4 unity_Projector;
             float4x4 unity_ProjectorClip;
 
             v2f vert(float4 vertex : POSITION)
             {
                 v2f o;
                 o.pos = UnityObjectToClipPos(vertex);
                 o.uv = mul(unity_Projector, vertex);
                 return o;
             }
 
             sampler2D _MainTex;
             fixed4 _Color;
 
             fixed4 frag(v2f i) : SV_Target
             {
                 fixed4 color = tex2Dproj(_MainTex, UNITY_PROJ_COORD(i.uv)) * _Color;
                 clip(1 + i.uv.z);
                 clip(1 - i.uv.z);
                 return color;
             }
             ENDCG
         }
     }
 }