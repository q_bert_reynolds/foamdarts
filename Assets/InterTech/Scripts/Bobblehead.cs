﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobblehead : MonoBehaviour
{
    Animator anim;
    public float bobbleDuration = 5;
    float lastMoveTime;

    Vector3 lastPosition;
    Vector3 lastRotation;

    void Awake () {
        anim = GetComponent<Animator>();
        lastPosition = transform.position;
        lastRotation = transform.eulerAngles;
        lastMoveTime = -bobbleDuration;
    }

    void Update () {
        float t = 1 - Mathf.Clamp01((Time.unscaledTime - lastMoveTime) / bobbleDuration);
        anim.speed = t;
        anim.SetLayerWeight(1, t);

        float dist = (lastPosition - transform.position).sqrMagnitude;
        float ang = Vector3.Angle(lastRotation, transform.eulerAngles);
        if (dist + ang > 0.5f) {
            lastMoveTime = Time.unscaledTime;
            lastPosition = transform.position;
            lastRotation = transform.eulerAngles;
        }
    }
}
