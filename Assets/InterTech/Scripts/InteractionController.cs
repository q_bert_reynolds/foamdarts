﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{
    public float interactDistance;
    public LayerMask interactionMask;
    Camera cam;

    void Start () {
        cam = Camera.main;
    }

    void Update () {
        Interactable interactable = null;
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, interactDistance, interactionMask)) {
            interactable = hit.collider.GetComponentInParent<Interactable>();
            if (!Interactable.interacting && interactable != null && Input.GetMouseButtonDown(0)) interactable.Interact();
        }
        UIController.UpdateReticle(interactable);
    }
}
