﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(BookGenerator))]
public class BookGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUI.changed) 
        {
            foreach (Object t in targets) ((BookGenerator)t).CreateMesh();
        }
    }
}
