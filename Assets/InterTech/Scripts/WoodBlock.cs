﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class WoodBlock : MonoBehaviour {

    public int seed = 0;
    public float size = 0.1f;

    Mesh mesh;

    [ContextMenu("Setup Mesh")]
    void SetupMesh () {

        Vector3[] vertices = {
            // front
			new Vector3( 1,-1, 1) * size,
			new Vector3( 1, 1, 1) * size,
			new Vector3(-1, 1, 1) * size,
			new Vector3(-1,-1, 1) * size,
            // back
			new Vector3(-1,-1,-1) * size,
			new Vector3(-1, 1,-1) * size,
			new Vector3( 1, 1,-1) * size,
			new Vector3( 1,-1,-1) * size,
            // left
			new Vector3(-1,-1, 1) * size,
			new Vector3(-1, 1, 1) * size,
			new Vector3(-1, 1,-1) * size,
			new Vector3(-1,-1,-1) * size,
            // right
			new Vector3( 1,-1,-1) * size,
			new Vector3( 1, 1,-1) * size,
			new Vector3( 1, 1, 1) * size,
			new Vector3( 1,-1, 1) * size,
            // top
			new Vector3(-1, 1,-1) * size,
			new Vector3(-1, 1, 1) * size,
			new Vector3( 1, 1, 1) * size,
			new Vector3( 1, 1,-1) * size,
            // bottom
			new Vector3(-1,-1, 1) * size,
			new Vector3(-1,-1,-1) * size,
			new Vector3( 1,-1,-1) * size,
			new Vector3( 1,-1, 1) * size,
		};

        Vector3[] normals = {
            // front
			Vector3.forward,
			Vector3.forward,
			Vector3.forward,
			Vector3.forward,
            // back
			Vector3.back,
			Vector3.back,
			Vector3.back,
			Vector3.back,
            // left
			Vector3.left,
			Vector3.left,
			Vector3.left,
			Vector3.left,
            // right
			Vector3.right,
			Vector3.right,
			Vector3.right,
			Vector3.right,
            // top
			Vector3.up,
			Vector3.up,
			Vector3.up,
			Vector3.up,
            // bottom
			Vector3.down,
			Vector3.down,
			Vector3.down,
			Vector3.down,
		};

        int[] triangles = new int[] {
             0, 1, 2, 0, 2, 3,
             4, 5, 6, 4, 6, 7,
             8, 9,10, 8,10,11,
            12,13,14,12,14,15,
            16,17,18,16,18,19,
            20,21,22,20,22,23,
        };

        mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.triangles = triangles;
        GetComponent<MeshFilter>().sharedMesh = mesh;

        UpdateUVs();
    }

    void UpdateUVs() {
        Vector2[] uvs = new Vector2[4 * 6];

        float size = 1f / 16f;
        for (int i = 0; i < 6; i++) {
            int x = Random.Range(0,16);
            int y = Random.Range(0,16);
            uvs[i * 4]     = new Vector2(x     * size, y    * size);
            uvs[i * 4 + 1] = new Vector2(x     * size, (y+1) * size);
            uvs[i * 4 + 2] = new Vector2((x+1) * size, (y+1) * size);
            uvs[i * 4 + 3] = new Vector2((x+1) * size, y * size);
        }

        MeshFilter filter = GetComponent<MeshFilter>();
        Mesh mesh = filter.sharedMesh;
        mesh.uv = uvs;
        filter.sharedMesh = mesh;
    }
}
