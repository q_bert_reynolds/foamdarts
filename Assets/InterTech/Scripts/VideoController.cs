﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoController : MonoBehaviour
{
    public string videoName;
    VideoPlayer videoPlayer;
    AudioSource audioSource;

    IEnumerator Start() {
        Renderer r = GetComponent<Renderer>();
        r.enabled = false;
        
        Application.runInBackground = true;
        videoPlayer = GetComponent<VideoPlayer>();
        audioSource = GetComponent<AudioSource>();

        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        videoPlayer.Stop();
        audioSource.Stop();

        videoPlayer.url = System.IO.Path.Combine("file://", Application.streamingAssetsPath, videoName);
        videoPlayer.controlledAudioTrackCount = 1;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);
        videoPlayer.Prepare();

        while (!videoPlayer.isPrepared) yield return null;

        r.enabled = true;
        videoPlayer.Play();
        audioSource.Play();
    }
}
