﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookController : MonoBehaviour
{
    public Transform lookTarget;
    [Range(0,1)] public float maxWeight = 0.95f;
    public float maxDist = 10;
    public float minDist = 2;
    public Transform leftEye;
    public Transform rightEye;

    Animator anim;
    void Start () 
    {
        anim = GetComponent<Animator>();
    }
    
    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if(lookTarget == null) return;

        float dist = (lookTarget.position - transform.position).magnitude;
        float weight = 1 - Mathf.Clamp01((dist-minDist)/(maxDist-minDist));
        anim.SetLookAtWeight(weight * maxWeight);
        anim.SetLookAtPosition(lookTarget.position);

        Vector3 diff = lookTarget.position-transform.position;
        Vector3 dir = diff.normalized;
        if (leftEye != null && Vector3.Dot(transform.forward, dir) > -0.25f)
        {
            leftEye.LookAt(lookTarget);
            rightEye.LookAt(lookTarget);
        }
    }    
}
